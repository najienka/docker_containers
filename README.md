# D2RQ Mapping in Docker



Build with Docker Compose:

docker-compose up -d (switches to a localised debian mirror in dockerfile to correct debian base image package errors/failures)



Build with Dockerfile


To access phpmyadmin and the d2r server, run with several exposed ports. e.g.:

	docker build -t d2rq-img .

	docker run -p 8082:80 -p 2020:2020 -i -t --name=d2rq-cont d2rq-img







To create a sample mapping:

	exec into the docker container, e.g: docker exec -it d2rq-cont bash





Create a database to be mapped with sql dump file and start d2rq server 'automatically':


	./map.sh <database_name> <filename> (database filename without .sql extension)




You can access the D2R server now under 

	http://localhost:2020 



and browse your data

	mapping file is in d2rq directory <mapping.ttl>




OR 






Create database via mysql server (or phpmyadmin via http://localhost:8082/phpmyadmin):

	mysql –defaults-file=/d2rq/mysql-opts.conf

	create database <dbname>;

	use <dbname>;

	create database tables   (or source /d2rq/filename.sql) 

	/q



create the mapping and start the server:

	./mapdb.sh <database_name>



You can access the D2R server now under 
	http://localhost:2020 



and browse your data


	mapping file is in d2rq directory <mapping.ttl>