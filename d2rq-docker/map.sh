#!/bin/sh



sql_script_file=$2
DATABASE_NAME=$1
MAPPING_FILE=/d2rq/mapping.ttl
  


mysql -h "localhost" -u root -ppassword -e "create database $DATABASE_NAME";

mysql -h "localhost" -u root -ppassword "$DATABASE_NAME" < /d2rq/$sql_script_file.sql



OUTPUT=$(/d2rq/generate-mapping -o $MAPPING_FILE -u root -p password jdbc:mysql://localhost/$DATABASE_NAME 2>&1)


if [ $? -eq 0 ];then
  chmod a+w $MAPPING_FILE

  echo "Mapping file for database $DATABASE_NAME created and server started."

sh /d2rq/d2r-server /d2rq/mapping.ttl
else
  rm $MAPPING_FILE

  echo "Error: $OUTPUT"
fi
